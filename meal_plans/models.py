from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField(auto_now=True)
    owner = models.ForeignKey(
        USER_MODEL, related_name="meal_plans",
        on_delete=models.CASCADE, null=True
        )
    # Upon logging in specific user can access this info,
    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plans"
        )
    # above takes data from recipes app., so it can be used in meal_plans

    def __str__(self):
        return f"{self.name} {self.date} {self.recipes} "
