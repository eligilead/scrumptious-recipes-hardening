from django.urls import path

from meal_plans.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanDetailView,
    # MealPlanDeleteView,
    # MealPlanUpdateView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),
    path("new/", MealPlanCreateView.as_view(), name="meal_plans_new"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plans_detail"),

]
