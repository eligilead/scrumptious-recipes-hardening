# from django.shortcuts import render

from meal_plans.models import MealPlan
from django.urls import reverse_lazy


from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
# Create your views here.


class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 2
    context_object_name = "list_of_meal_plans"


class MealPlanCreateView(CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("meal_plans/list.html")


class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"
    context_object_name = "details_of_meal_plan"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     return context


class MealPlanDeleteView(DeleteView):
    pass


class MealPlanUpdateView(UpdateView):
    pass
